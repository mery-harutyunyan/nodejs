const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const {v4: uuidv4} = require("uuid");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const path = './data/data.json';

const saveUserData = (data) => {
    let stringify = JSON.stringify(data);

    fs.writeFileSync(path, stringify);
}

const getUserData = () => {
    let json = fs.readFileSync(path);

    return JSON.parse(json);
}

app.get('/users', (request, response) => {
    fs.readFile(path, "utf8", (error, data) => {
        if (error)
            throw error;

        response.send(JSON.parse(data));
    })
})


app.post('/users', (request, response) => {
    let allData = getUserData();
    const userId = uuidv4();
    allData[userId] = request.body;
    saveUserData(allData);

    response.send({success: true, message: 'User added successfully'})
})

app.put('/users/:id', (request, response) => {
    let allData = getUserData();
    const userId = request.params['id'];
    allData[userId] = request.body;
    saveUserData(allData);

    response.send({success: true, message: 'User updated successfully'});
})

app.delete('/users/:id', (request, response) => {
    let allData = getUserData();
    const userId = request.params['id'];
    delete allData[userId];
    saveUserData(allData);

    response.send({success: true, message: 'User deleted successfully'});
})

app.listen(8080, () => {
    console.log("Server started");
})